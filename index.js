const first = document.getElementById("first");
const second = document.getElementById("second");
const third = document.getElementById("third");
const section = document.querySelectorAll("section");
const btn = document.getElementById("btn");
let currentDiv = undefined;
let current = false;

const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

const cleanDivs = () => {
  first.innerText = "";
  second.innerText = "";
  third.innerText = "";
};

const createDivs = () => {
  cleanDivs();
  let input = document.getElementById("input").value;
  if (input > 7 || input < 3) {
    alert("Just numbers between 3 and 7, tyyyy S2")
  } else {

    for (let i = 1; i <= input; i++) {
      let div = document.createElement("div");  
  
      div.style.width = `${300-(30*i)}px`;
      div.style.height = `${30}px`;
      let colorOne = getRandomInt(0,255);
      let colorTwo = getRandomInt(0,255);
      let colorThree = getRandomInt(0,255);
      div.style.backgroundColor = `rgb(${colorOne},${colorTwo},${colorThree})`;
  
      first.appendChild(div);
    }
  }
};

const errorCondition = (element,div) => {
    if (element === null) {
      return true;
    } else if (element.clientWidth < div.clientWidth) {
      return false;
    } else {
      return true;
  }
}

const  game = evt => {
  let currentSection = evt.currentTarget;
  let count = currentSection.childElementCount;

  if (count === 0 && !currentDiv) {
    alert("Don't have any disk's to be moved!");

  } else if (current) {
    let nextElement = currentSection.lastElementChild;

    if (errorCondition(nextElement,currentDiv)) {
        currentSection.appendChild(currentDiv);
        currentDiv = undefined;
        current = false;
        isWin();
    } else {
        alert("Ops!, you can't place a larger disk in a smaller one!");
        current = false;
    }

  } else {
    currentDiv = evt.currentTarget.lastElementChild;
    current = true;
  }

};

const reset = () => {
    const divs = document.querySelectorAll('#third div');
    divs.forEach(element => {
        first.appendChild(element);
    });
}

const isWin = () => {
  if (first.firstElementChild === null && second.firstElementChild === null) {
    alert("Congratulation You Won!");
    reset();
  }
};

btn.addEventListener("click", createDivs);
section.forEach(element => element.addEventListener('click', game));